import blockToast from "./toast.vue"
blockToast.install = function (Vue) {
    Vue.component(blockToast.name, blockToast)
}
export default blockToast;